#include<stdio.h>
int main()
{
	float c;
	printf("\nEnter temperature in celsius : ");
	scanf("%f", &c);
	float f = (c * 9 / 5) + 32.0;
	printf("Temperature in fahrenheit : %.2f\n", f);
	return 0;
}
